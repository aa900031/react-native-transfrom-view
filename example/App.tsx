import _ from 'lodash';
import React, { Component } from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';
import { Constants } from 'expo';
import { TransfromView, TransfromImage } from './dist';
import Swiper from './components/Swiper';

interface Prop {
  
}

interface State {
  scene: Scene
  data1: Data1[]
  data2: Data2[]
}

type Scene = 'view'
  | 'image'
  | 'swiper';

type Data1 = {
  background: string
  text: string
}

type Data2 = {
  width: number
  height: number
}

class App extends Component<Prop, State> {
  state: State = {
    scene: 'view',
    data1: [
      { background: 'red', text: 'page 1' },
      { background: 'blue', text: 'page 2' },
      { background: 'yellow', text: 'page 3' },
    ],
    data2: [
      { width: 350, height: 500 },
      { width: 500, height: 500 },
      { width: 300, height: 900 },
      { width: 450, height: 600 },
    ],
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header} >
          <Button color={this.state.scene === 'view' ? 'red' : undefined} title="TransfromView" onPress={() => this._handleButtonPress('view')} />
          <Button color={this.state.scene === 'image' ? 'red' : undefined} title="TransfromImage" onPress={() => this._handleButtonPress('image')} />
          <Button color={this.state.scene === 'swiper' ? 'red' : undefined} title="Swiper" onPress={() => this._handleButtonPress('swiper')} />
        </View>
        {this._renderByScene()}
      </View>
    );
  }

  _renderByScene = () => {
    switch (this.state.scene) {
      case 'view':
        return (
          <TransfromView wrapperStyle={{ flex: 1, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center' }}>
            <Text style={styles.text} >Hello World</Text>
          </TransfromView>
        );
      case 'image':
        return (
          <TransfromImage source={{ uri: 'https://via.placeholder.com/300x1250' }} />
        );
      case 'swiper':
        return (
          <Swiper data={this.state.data1} renderItem={this._renderItem1} />
        )
      default:
        return null
    }
  }

  _renderItem1 = (info: { item: Data1; index: number; }) => {
    return (
      <View style={[styles.item, { backgroundColor: info.item.background }]} >
        <Text>{info.item.text}</Text>
      </View>
    );
  }

  _handleButtonPress = (scene: Scene) => {
    this.setState({ scene })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
  },
  header: {
    paddingTop: Constants.statusBarHeight,
    justifyContent: 'space-around',
    backgroundColor: '#ECEFF1',
    flexDirection: 'row'
  },
  text: {
    fontSize: 18,
    color: 'white',
  },
  item: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});

export default App;