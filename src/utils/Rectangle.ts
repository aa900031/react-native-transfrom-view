export default class Rectangle {
  private _left: number = 0
  private _top: number = 0
  private _right: number = 0
  private _bottom: number = 0

  constructor(left?: number, top?: number, right?: number, bottom?: number) {
    if (left) {
      this.left = left;
    }
    if (top) {
      this.top = top;
    }
    if (right) {
      this.right = right;
    }
    if (bottom) {
      this.bottom = bottom;
    }
  }

  get left() {
    return this._left;
  }

  get top() {
    return this._top;
  }

  get right() {
    return this._right;
  }

  get bottom() {
    return this._bottom;
  }

  set left(val: number) {
    if (val !== undefined && typeof val === 'number' && this._left !== val) {
      this._left = val;
    }
  }

  set top(val: number) {
    if (val !== undefined && typeof val === 'number' && this._top !== val) {
      this._top = val;
    }
  }

  set right(val: number) {
    if (val !== undefined && typeof val === 'number' && this._right !== val) {
      this._right = val;
    }
  }

  set bottom(val: number) {
    if (val !== undefined && typeof val === 'number' && this._bottom !== val) {
      this._bottom = val;
    }
  }

  get width(): number {
    return this.right - this.left;
  }

  get height(): number {
    return this.bottom - this.top;
  }

  get centerX() {
    return (this.left + this.right) / 2;
  }

  get centerY() {
    return (this.top + this.bottom) / 2;
  }

  clone() {
    return new Rectangle(this.left, this.top, this.right, this.bottom);
  }

  offset(dx: number, dy: number) {
    this.left += dx;
    this.right += dx;
    this.top += dy;
    this.bottom += dy;

    return this;
  }

  transform(toRect: Rectangle) {
    const scale = toRect.width / this.width;

    return {
      scale,
      translateX: (toRect.centerX - this.centerX) / scale,
      translateY: (toRect.centerY - this.centerY) / scale,
    };
  }

  transformed(transform: { scale: number, translateX: number, translateY: number, pivotX?: number, pivotY?: number }): Rectangle {
    if (transform.pivotX !== undefined && transform.pivotY !== undefined) {
      let dx = (transform.scale - 1) * (transform.pivotX - this.centerX);
      let dy = (transform.scale - 1) * (transform.pivotY - this.centerY);

      return this.transformed({ scale: transform.scale, translateX: transform.translateX, translateY: transform.translateY }).offset(-dx, -dy);
    }
    let width = this.width * transform.scale;
    let height = this.height * transform.scale;
    let centerX = this.centerX + transform.translateX * transform.scale;
    let centerY = this.centerY + transform.translateY * transform.scale;

    return new Rectangle(
      centerX - width / 2,
      centerY - height / 2,
      centerX + width / 2,
      centerY + height / 2
    );
  }

  aligned(target: Rectangle): Rectangle {
    let dx = 0, dy = 0;

    if (this.width > target.width) {
      if (this.left > target.left) {
        dx = target.left - this.left;
      } else if (this.right < target.right) {
        dx = target.right - this.right;
      }
    } else {
      dx = target.centerX - this.centerX;
    }

    if (this.height > target.height) {
      if (this.top > target.top) {
        dy = target.top - this.top;
      } else if (this.bottom < target.bottom) {
        dy = target.bottom - this.bottom;
      }
    } else {
      dy = target.centerY - this.centerY;
    }

    return this.clone().offset(dx, dy);
  }

  fitCenter(aspectRatio: number): Rectangle {
    let width = this.width;
    let height = this.height;
    let viewAspectRatio = width / height;

    if (aspectRatio > viewAspectRatio) {
      height = width / aspectRatio;
    } else {
      width = height * aspectRatio;
    }

    return new Rectangle(
      this.centerX - width / 2,
      this.centerY - height / 2,
      this.centerX + width / 2,
      this.centerY + height / 2
    );
  }

  availableTranslateSpace(target: Rectangle) {
    return {
      left: target.left - this.left,
      right: this.right - target.right,
      top: target.top - this.top,
      bottom: this.bottom - target.bottom
    };
  }

  equal(otherRectangle: Rectangle) {
    return this.left === otherRectangle.left && this.top === otherRectangle.top && this.right === otherRectangle.right && this.bottom && otherRectangle.bottom;
  }
}