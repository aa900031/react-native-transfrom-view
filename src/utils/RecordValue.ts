export type ChangeHandler<T> = (curr: T, prev: T) => void;

export default class RecordValue<T> {
  private _curr: T;
  private _prev: T;
  private _listener: ChangeHandler<T>[] = [];
  
  constructor(val: T) {
    this._curr = this._prev = val;
  }

  get value() {
    return this._curr;
  }

  set value(val) {
    this._prev = this._curr;
    this._curr = val;
    this.emitChange(this._curr, this._prev);
  }

  private emitChange(curr: T, prev: T) {
    this._listener.forEach(handler => handler(curr, prev));
  }

  addListener(handler: ChangeHandler<T>): number {
    const nextIndex = this._listener.length;
    this._listener.push(handler)
    return nextIndex;
  }

  removeListener(index: number): boolean {
    const prevLength = this._listener.length;
    this._listener.splice(index, 1);

    return prevLength - 1 == this._listener.length;
  }

  removeAllListener(): boolean {
    this._listener.length = 0;
    return this._listener.length == 0;
  }
}