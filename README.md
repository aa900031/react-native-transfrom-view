# react-native-transfrom-view

自由的縮放/移動你的View

## 安裝
```shell
npm install react-native-transfrom-view
# or
yarn add react-native-transfrom-view 
```

# 組件-TransfromView
基礎元件，縮放/移動

## 使用
```js
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { TransfromView } from 'react-native-transfrom-view';

class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TransformView wrapperStyle={styles.wrapper}>
          <Text style={styles.text} >Hello World</Text>
        </TransformView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {
    flex: 1,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default App;
```

## TransfromView Props
- [View.props...](https://facebook.github.io/react-native/docs/view.html#props)
- [containerStyle](###containerStyle)
- [wrapperStyle](###wrapperStyle)
- [scopeStyle](###scopeStyle)
- [minScale](###minScale)
- [maxScale](###maxScale)
- [useNativeDriver](###useNativeDriver)
- [enabled](###enabled)
- [waitFor](###waitFor)
- [simultaneousHandlers](###simultaneousHandlers)
- [onContentAlign](###onContentAlign)
- [onGesturePanEnableChange](###onGesturePanEnableChange)

### containerStyle
- 最外層的樣式
- View style

### wrapperStyle
- 嚮應動畫的樣式
- Animated.View style

### scopeStyle
- 手勢作用域樣式
- View style

### minScale
- 最小縮放
- number

### maxScale
- 最大縮放
- number

### useNativeDriver
- 是否使用原生動畫
- boolean

### enabled
- 啟用手勢
- boolean

### waitFor
- 等待其他GestureHandler
- React.Ref | React.Ref[]

### simultaneousHandlers
- 同步其他GestureHanlder
- React.Ref | React.Ref[]

### onContentAlign
- 內容靠近邊界
- Function
- 參數: align: object
  - top: boolean
  - left: boolean
  - right: boolean
  - bottom: boolean

### onGesturePanEnableChange
- GesturePanHandler 啟用監控
- Function
- 參數: enable: boolean


# 組件-TransfromImage
顯示圖片，可縮放/移動，當傳入圖片參數為uri時，會顯示載入指示器

## 使用
```js
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { TransfromImage } from 'react-native-transfrom-view';

class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TransfromImage source={{ uri: 'https://via.placeholder.com/350x150' }} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
```

## TransfromImage Props
- [Image.props...](https://facebook.github.io/react-native/docs/image.html#props)
- [TransfromView.props...](##TransfromView\ Props)
