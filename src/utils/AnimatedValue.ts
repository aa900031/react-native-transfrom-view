import { Animated } from 'react-native';
import RecordValue, { ChangeHandler } from './RecordValue';

export type ListenerType = 'value' | 'valueState' | 'offset' | 'offsetState';

export default class AnimatedValue {
  animate = new Animated.Value(0);

  private _value: RecordValue<number>;
  private _valueState: RecordValue<number>;
  private _offset: RecordValue<number>;
  private _offsetState: RecordValue<number>;

  constructor (value: number) {
    this._value = new RecordValue(value);
    this._valueState = new RecordValue(value);
    this._offset = new RecordValue(0);
    this._offsetState = new RecordValue(0);

    this.animate.setValue(this._value.value);
    this.animate.setOffset(this._offset.value);
  }

  get value() {
    return this._value.value;
  }

  get offset() {
    return this._offset.value;
  }

  get valueState() {
    return this._valueState.value;
  }

  get offsetState() {
    return this._offsetState.value;
  }

  set value(val: number) {
    this._value.value = val;
    this.animate.setValue(this._value.value);
  }

  set valueState(val: number) {
    this._valueState.value = val;
  }

  set valueWithoutAnimate(val: number) {
    this._value.value = val;
  }

  set offset(val: number) {
    this._offset.value = val;
    this.animate.setOffset(this._offset.value);
  }

  set offsetState(val: number) {
    this._offsetState.value = val;
  }

  set offsetWithoutAnimated(val: number) {
    this._offset.value = val;
  }

  addListener(type: ListenerType, handler: ChangeHandler<number>): number {
    if (type === 'value') {
      return this._value.addListener(handler);
    } else if (type === 'valueState') {
      return this._valueState.addListener(handler);
    } else if (type === 'offset') {
      return this._offset.addListener(handler);
    } else if (type === 'offsetState') {
      return this._offsetState.addListener(handler);
    }

    return -1;
  }

  removeListener(type: ListenerType, index: number): boolean {
    if (type === 'value') {
      return this._value.removeListener(index);
    } else if (type === 'valueState') {
      return this._valueState.removeListener(index);
    } else if (type === 'offset') {
      return this._offset.removeListener(index);
    } else if (type === 'offsetState') {
      return this._offsetState.removeListener(index);
    }

    return false;
  }

  removeAllListener(type: ListenerType): boolean {
    if (type === 'value') {
      return this._value.removeAllListener();
    } else if (type === 'valueState') {
      return this._valueState.removeAllListener();
    } else if (type === 'offset') {
      return this._offset.removeAllListener();
    } else if (type === 'offsetState') {
      return this._offsetState.removeAllListener();
    }

    return false;
  }
}